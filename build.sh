#!/bin/bash

npm install
./node_modules/.bin/grunt
cp index.html fontane.html dhsummit.html view.html build/mirador/
#chgrp www-data build/mirador/index.html

VERSION=$(jq -r .version package.json)
BUILD_NUMBER=$(date +"%Y%m%d%H%M%S")

fpm -t deb -a noarch -s dir --name textgrid-mirador \
                            --description='Mirador build for textgrid' \
                            --maintainer='DARIAH-DE <info@de.dariah.eu>' \
                            --vendor='DARIAH-DE' \
                            --url='http://textgrid.de' \
                            --iteration ${BUILD_NUMBER} \
                            --version ${VERSION} \
                            -x ".git**" -x "**/.git**" -x "**/.hg**" -x "**/.svn**" -x ".buildinfo" -x "**/*.deb" \
                            --prefix /var/www/nginx-root/textgridrep.de/iiif/mirador \
                            -C build/mirador .

exit 0
